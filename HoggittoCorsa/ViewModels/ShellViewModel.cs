﻿using System;
using System.Collections.Generic;
using System.Text;
using Caliburn.Micro;

namespace HoggittoCorsa.ViewModels
{
    public class ShellViewModel : Conductor<object>
    {
        public IWindowManager WindowManager { get; }

        public ShellViewModel(IWindowManager windowManager)
        {
            WindowManager = windowManager;
            ActivateItem(new MainWindowViewModel());
        }
    }
}
