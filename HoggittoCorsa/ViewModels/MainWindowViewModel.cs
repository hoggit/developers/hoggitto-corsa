using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Windows;
using Amazon.S3;
using Amazon.S3.Model;
using Caliburn.Micro;
using Microsoft.Win32;

namespace HoggittoCorsa.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        private string _contentDirectory;
        private string _accessKey = "";
        private string _secret = "";
        private string _endpoint = "https://sfo2.digitaloceanspaces.com";
        private string _bucketName = "mizery";
        private string _objectKey = "HoggittoCorsa/HoggittoCorsa.zip";
        private AmazonS3Client _client;

        private string _statusText;
        public string StatusText
        {
            get => _statusText;
            set => Set(ref _statusText, value);
        }

        private string _acFolder;

        public string ACFolder
        {
            get => _acFolder;
            set => Set(ref _acFolder, value);
        }

        public MainWindowViewModel()
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;

            StatusText = "Vroom vroom!";
            var s3Config = new AmazonS3Config
            {
                ServiceURL = _endpoint,
            };

            var creds = new Amazon.Runtime.BasicAWSCredentials(_accessKey, _secret);
            _client = new AmazonS3Client(creds, s3Config);

        }

        public void BrowseClick()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Assetto Corsa exe|AssettoCorsa.exe";
            if (ofd.ShowDialog() == true)
            {
                ACFolder = ofd.FileName;
                _contentDirectory = ofd.FileName.Replace("AssettoCorsa.exe", "");
                StatusText = $"Content directory set to {_contentDirectory}";
            }
        }

        public void GetModPackClick()
        {
            getModPack();
        }


        private async void getModPack()
        {
            StatusText = "Fetching modpack...";
            var objectRequst = new GetObjectRequest
            {
                BucketName = _bucketName,
                Key = _objectKey
            };

            try
            {

                var response = await _client.GetObjectAsync(objectRequst).ConfigureAwait(false);
                var zipPath = Path.Join(_contentDirectory, @"HogittoCorsa.zip");
                using (var fileStream = File.Create(zipPath))
                using (Stream responseStream = response.ResponseStream)
                {
                    responseStream.CopyTo(fileStream);
                    StatusText = "Decompressing modpack...";
                }
                ZipFile.ExtractToDirectory(zipPath, _contentDirectory, true);
                StatusText = "Modpack is up to date!";
            }
            catch (Exception ex)
            {
                MessageBox.Show($"We crashed:{Environment.NewLine}{ex.GetType()}: {ex.Message}");
                throw;
            }
        }

    }
}
